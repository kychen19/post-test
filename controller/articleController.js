const {Article} = require('../models')

module.exports = {
    index: (req,res) => {
        // res,send('index article')
        Article.findAll({
        }).then(articles => {
            if(articles.length !== 0){
                res.render('articles/index', {
                  articles
                })
            }else{
                res.json({
                    'status' : 400,
                    'message' : 'data kosong'
                })
            }
         })
    },
    create: (req, res) => {
        const{
            title,
            body,
            approved
        } = req.body;

        Article.create({
            title,
            body,
            approved : false
        }).then(articles => {
            res.render('articles/index', {
                articles

            })
        })
    },
    update: (req,res) => {
        const userId = req.params.id;
        const query ={
            where: {id: userId}
        }
        Article.update({
            approved: true
        }, query).then(res =>{
            res.json({
                'status': 200,
                'message': 'berhasil diupdate',
                'data' : userId
            })
        }).catch(err => {
            res,json({
                'status' : 400,
                'message' : 'data gagal diupdate'
            });

        })
    },
    show: (req,res) => {
        const userId = req.params.id;

        Article.findOne({
            where: {id: userId}
        }).then(data => {
            res.json({
                'status': '200',
                'message': 'data ditemukan',
                'data' : data
            })
        }).catch(err => {
            res.json({
                'status' : 400,
                'message': 'data tidak ditemukan'
            });
        })
    },
    delete: (req,res) =>{
        const userId = req.params.id;

        Article.destroy({
            where: {id: userId}
        }).then(data => {
            res.json({
                'status' : 200,
                'mesaage': 'data berhasil dihapus',
                'data' : userId
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message' : 'data gagal dihapus'
            })
        })
    },
    createArticle: (req, res) => {
        res.render('./articles/create')
    },
    detail: (req, res) => {
        Article.findOne({
            where:{
                id: req.params.id
            }
        })
        .then(article => {
            res.render('./articles/detail', {article})
        })
    }

}