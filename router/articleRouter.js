const router = require('express').Router()
const articleController = require('../controller/articleController')

router.get('/', articleController.index)
router.get('/detail/:id', articleController.detail)

router.get('/create', articleController.createArticle)
router.post('/create', articleController.create)

router.get('/id', articleController.show)
router.patch('/:id', articleController.update)
router.delete('/delete/:id', articleController.delete)

module.exports = router 